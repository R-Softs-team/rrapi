<?php

namespace rrapi;

use rrapi\interfaces\RRAPIInterface;
use rrapi\models\Account\Account;


class RRAPI implements RRAPIInterface
{
    protected $access_token;
    protected $version = "1.0";

    public function __construct(?string $access_token = null)
    {
        if (!empty($access_token))
            $this->access_token = $access_token;
    }

    public function getAccount(): ?Account
    {
        return new Account();
    }

    public function request()
    {

    }
}