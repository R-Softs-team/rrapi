<?php
/**
 * Created by IntelliJ IDEA.
 * User: rustam
 * Date: 07.12.18
 * Time: 14:45
 */

namespace rrapi;


class Request
{
    public static function send($method ,$url , array $data = null, string $query=null): array {
        $url .= (!empty($data) || !empty($query))? "?": null;
        $url .=!empty($data)? "type=json" : null;
        $url .=!empty($query)? "query=".$query : null;

        $curl = curl_init();
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
//        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
        curl_setopt($curl,CURLOPT_URL, $url);
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST, $method);
        if (!empty($data)) curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($data));

        curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        curl_setopt($curl,CURLOPT_HEADER,false);
//        curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt');
//        curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt');
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);

        $out=curl_exec($curl);
        $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ( $code == 200 || $code == 204 ) {
            $res=json_decode($out,true);
            return !empty($res)? $res : [];
        }
        return [];
    }
}