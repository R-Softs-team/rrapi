<?php

namespace rrapi\interfaces;

use rrapi\models\Account\Account;

interface RRAPIInterface
{
    public function getAccount(): ?Account;
}