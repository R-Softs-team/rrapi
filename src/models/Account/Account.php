<?php
/**
 * Created by IntelliJ IDEA.
 * User: pyctam4ik
 * Date: 06.12.18
 * Time: 22:42
 */

namespace rrapi\models\Account;

use rrapi\models\Model;
use rrapi\models\Account\Interfaces\Account as AccountInterface;

class Account extends Model implements AccountInterface
{
    private $info;

    public function info(): ?Info
    {
        if (empty($this->info))
            $this->info = new Info($this->get('account.info'));
        return $this->info;
    }

    public function bill(): ?Bill
    {
        return null;
    }

    public function getPermissions(): ?Permissions
    {
        return null;
    }

    public function invoices(): ?Invoices
    {
        return null;
    }

}