<?php
/**
 * Created by IntelliJ IDEA.
 * User: rustam
 * Date: 07.12.18
 * Time: 10:50
 */

namespace rrapi\models\Account;

class Info
{
    public $email;
    public $phone;
    public $name;
    public $balance;

    public $emptyFields = [];

    public function __construct(?array $data = [])
    {
        foreach ($data??[] as $field=>$value) {
            switch ($field) {
                case 'email' : $this->email = $value; break;
                case 'phone' : $this->phone = $value; break;
                case 'name' : $this->name = $value; break;
                case 'balance' : $this->balance = $value; break;
                default: $this->emptyFields[$field] = $value;
            }
        }
    }
}