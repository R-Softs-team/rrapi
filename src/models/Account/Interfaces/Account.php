<?php
/**
 * Created by IntelliJ IDEA.
 * User: rustam
 * Date: 07.12.18
 * Time: 10:55
 */

namespace rrapi\models\Account\Interfaces;

use rrapi\models\Account\Bill;
use rrapi\models\Account\Info;
use rrapi\models\Account\Invoices;
use rrapi\models\Account\Permissions;

interface Account
{
    public function info(): ?Info;

    public function bill(): ?Bill;

    public function getPermissions(): ?Permissions;

    public function invoices(): ?Invoices;

}