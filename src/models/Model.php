<?php
/**
 * Created by IntelliJ IDEA.
 * User: pyctam4ik
 * Date: 06.12.18
 * Time: 22:44
 */

namespace rrapi\models;

abstract class Model
{
    protected $token;
    protected $version;

    /**
     * @param string $method
     * @return array
     * @throws \Exception
     */
    public function get(string $method): array
    {
        $request_params = [ 'v' => $this->version, 'access_token' => $this->token ];
        $get_params = http_build_query($request_params);
        $result = json_decode(file_get_contents('https://rosreestr.net/api/method/'.$method.'?'. $get_params), true);

        if (!empty($result['error'])) throw new \Exception("ROSREESTR RESPONSE ERROR[". $result['error']['error_code']. "] - ". $result['error']['error_msg']);
        return $result->response->data;
    }
}